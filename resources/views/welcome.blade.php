<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset ('css/adminlte.min.css')}}">
        <script src="{{ asset('js/app.js') }}"></script>
        <title>DataKaryawan</title>
    </head>
    <body>
        <div class="col-md-8 col-md-offset-2">
            <div class="page-header">
                <h1>Input Data Karyawan</h1>
                <a href="#" data-toggle="modal" data-target="#modal-add">Tambah Data</a>
            </div>
            <table class="table">
                <thead>
                    <th>ID</th>
                    <th>nama</th>
                    <th>alamat</th>
                    <th>jabatan</th>
                    <th>opsi</th>
                </thead>
                <tbody>
                @foreach($dta as $data)
                    <tr>
                        <td>{{ $data->id }}</td>
                        <td>{{ $data->nama }}</td>
                        <td>{{ $data->alamat }}</td>
                        <td>{{ $data->nama_jabatan }}</td>
                        <td><div class="btn-group"><a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-id="{{ $data->id }}" data-nm="{{ $data->nama }}" data-almt="{{ $data->alamat }}" data-jbt="{{ $data->kd_jabatan }}" data-target="#modal-edit" >UBAH</a> <a href="#" class="btn btn-danger btn-sm" data-toggle="modal" data-kd="{{ $data->id }}" data-target="#modal-delete">HAPUS</a></div></td>
                    </tr>
                @endforeach  
                </tbody>
            </table>
        </div>
        <div class="modal fade" id="modal-add">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Tambah Karyawan</h4>
                        <button type="button" class="close" data-dismiss="modal">
                            <span>×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <form class="form-horizontal" method="post" enctype="multipart/form-data">
                                {{csrf_field()}}
                                    <div class="form-group">
                                        <label>Nama</label>
                                        <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama Karyawan" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <textarea type="text" class="form-control" name="alamat" placeholder="Masukkan Alamat Karyawan" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Jabatan</label>
                                        <select class="form-control" name="jabatan">
                                            <option value="">--Pilih--</option>
                                            <option value="1">Manager</option>
                                            <option value="2">Direktur</option>
                                            <option value="3">Operator</option>
                                            <option value="4">Karyawan</option>
                                        </select>
                                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"></span>Batal</button>
                        <button type="submit" class="btn btn-success"></span>Tambah</a></button>             
                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-delete">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Hapus Pelanggan</h4>
                        <button type="button" class="close" data-dismiss="modal">
                            <span>×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form method="POST" action="{{ route('kw.destroy','kr') }}" enctype="multipart/form-data">  
                            {{method_field('delete')}}
                                {{csrf_field()}}
                                    <p class="text-center">Apakah anda yakin ingin menghapus ?</p>
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" id="kode" name="kode" required>
                                    </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" name="submit2" class="btn btn-danger">Hapus</button>
                    </div>
                        </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-edit">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Ubah Karyawan</h4>
                        <button type="button" class="close" data-dismiss="modal">
                            <span>×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <form class="form-horizontal" method="post" action="{{ route('kw.update','kw') }}" enctype="multipart/form-data">
                                {{method_field('patch')}}   
                                    {{csrf_field()}}
                                    <div class="form-group">
                                        <input type="hidden" class="form-control" id="kode" name="kode" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="namaContoh">Nama</label>
                                        <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama Karyawan" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="namaContoh">Alamat</label>
                                        <textarea type="text" class="form-control" id="alamat" name="alamat" placeholder="Masukkan Alamat Karyawan" required></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="dendaContoh">Jabatan</label>
                                        <select class="form-control" id="jabatan" name="jabatan">
                                            <option value="1">Manager</option>
                                            <option value="2">Direktur</option>
                                            <option value="3">Operator</option>
                                            <option value="4">Karyawan</option>
                                        </select>
                                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"></span>Batal</button>
                        <button type="submit" name="submit" id="submit" class="btn btn-success"></span>Ubah</a></button>          
                    </div>
                    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $('#modal-edit').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var id = button.data('id');       
                var nm = button.data('nm');                
                var almt = button.data('almt'); 
                var jbt = button.data('jbt');
                var modal = $(this);
                modal.find('#kode').val(id);
                modal.find('#nama').val(nm);
                modal.find('#alamat').val(almt);
                modal.find('#jabatan').val(jbt);
            });
            $('#modal-delete').on('show.bs.modal', function(event){
                var b = $(event.relatedTarget);
                var kode = b.data('kd');
                var modal = $(this);
                modal.find('#kode').val(kode);
            });
        </script>
    </body>
</html>
