<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\karyawan;

class AppController extends Controller
{
    public function index()
    {
        $dta = DB::select("select * from karyawan, jabatan where jabatan.kd_jabatan=karyawan.kd_jabatan");
        return view('welcome',compact('dta'));
    }
    public function store(Request $request)
    {
        $kw = new karyawan;
        $kw->nama = $request->get('nama');
        $kw->alamat = $request->get('alamat');
        $kw->kd_jabatan = $request->get('jabatan');
        $kw->save();    
        return back();
    }
    public function destroy(Request $request)
    {
        $kw = karyawan::findOrfail($request->kode);
        $kw->delete();
        return back();
    }
    public function update(Request $request)
    {
        $kw = karyawan::findOrfail($request->kode);  
        $kw->nama = $request->get('nama');
        $kw->alamat = $request->get('alamat');
        $kw->kd_jabatan = $request->get('jabatan');
        $kw->update();
        return back();
    }
}
