<?php

use Illuminate\Database\Seeder;

class karyawan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['nama'=>'Supriadi', 'alamat'=> 'jl asembagus', 'kd_jabatan'=> 1],
            ['nama'=>'Fatima', 'alamat'=> 'jl seroja', 'kd_jabatan'=> 2],
            ['nama'=>'Madadi', 'alamat'=> 'jl pattimura', 'kd_jabatan'=> 3],
            ['nama'=>'Dian', 'alamat'=> 'jl kalibagus', 'kd_jabatan'=> 4]
        ];
        DB::table('karyawan')->insert($data);
    }
}
