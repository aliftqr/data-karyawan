<?php

use Illuminate\Database\Seeder;

class jabatan extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['nama_jabatan'=> 'Manager'],
            ['nama_jabatan'=> 'Direktur'],
            ['nama_jabatan'=> 'Operator'],
            ['nama_jabatan'=> 'Karyawan']
        ];
        DB::table('jabatan')->insert($data);
    }
}
